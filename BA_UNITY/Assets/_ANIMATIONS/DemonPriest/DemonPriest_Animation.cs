﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemonPriest_Animation : MonoBehaviour {
    [SerializeField]
    private EnemyBehavior _enemyBehavior;
    [SerializeField]
    private Animator _animator;

    private void Update()
    {
        _animator.SetFloat("Speed", _enemyBehavior._currentSpeed);
    }
}