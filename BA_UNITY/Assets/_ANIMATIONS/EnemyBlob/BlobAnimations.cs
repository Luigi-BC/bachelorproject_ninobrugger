﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlobAnimations : MonoBehaviour {


    [SerializeField]
    private EnemyBehavior _enemyBehavior;
    [SerializeField]
    private Animator _animator;

	// Use this for initialization
	private void Start ()
    {
        _enemyBehavior.EventAttack += Attack;
	}

    private void OnDestroy()
    {
        _enemyBehavior.EventAttack -= Attack;
    }

    private void Update()
    {
        _animator.SetFloat("Speed", _enemyBehavior._currentSpeed);
    }

    private void Attack()
    {
        _animator.SetTrigger("Attack");
    }

    public void ActivateCollider()
    {
        _enemyBehavior.ActivateHitCollider();
    }

    public void DeactivateCollider()
    {
        _enemyBehavior.DeactivateHitCollider();
    }

}
