﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

[CustomEditor(typeof(CommunicationLevels))]
public class CommunicationLevelsEditor : Editor {

    private ReorderableList list;

    private void OnEnable()
    {
        list = new ReorderableList(serializedObject, serializedObject.FindProperty("_communicationEvents"), true, true, true, true);

        list.drawElementCallback =
            (Rect rect, int index, bool isActive, bool isFocused) =>
            {
                var element = list.serializedProperty.GetArrayElementAtIndex(index);
                rect.y += 2;

                SerializedProperty sProp = element.FindPropertyRelative("_event");

                EditorGUI.PropertyField(
                    new Rect(rect.x, rect.y, rect.width -20, 400),
                    sProp, GUIContent.none);

            };



        list.elementHeightCallback = (int index) =>
        {
            Repaint();

            float height = 0;

            try
            {
                var element = list.serializedProperty.GetArrayElementAtIndex(index);
                height = EditorGUI.GetPropertyHeight(element);
            } catch( ArgumentOutOfRangeException e)
            {
                Debug.LogWarning(e);
            }


            return height;
        };

    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        
        DrawDefaultInspector();
        list.DoLayoutList();
        serializedObject.ApplyModifiedProperties();
    }

}
