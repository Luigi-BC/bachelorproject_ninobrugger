﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    [SerializeField]
    private float _damage;
    [SerializeField]
    private GameObject _hitFXPrefab;

    [SerializeField]
    private LayerMask _layerMask;

	// Use this for initialization
	void Start () {
        Invoke("KillProjectile", 3f);
        CheckTouchingLayer();
	}

    private void CheckTouchingLayer()
    {
        if (GetComponent<Collider2D>().IsTouchingLayers(_layerMask))
        {
            KillProjectile();
        }
    }

    private void KillProjectile()
    {
        Instantiate(_hitFXPrefab, transform.position, Quaternion.identity, null);
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.isTrigger == true)
        {
            return;
        }

        if(collision.gameObject.layer == 13)
        {
            return;
        }
        if(collision.gameObject.layer ==  11)
        {
            IHealth ihealth = collision.gameObject.GetComponent<IHealth>();
            ihealth.OnHit(_damage);
        }
        KillProjectile();
    }


}
