﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootController : MonoBehaviour {
    [Serializable]
    private struct WeaponStruct
    {
        public GameObject weaponObject;
        public float fireStrength;
        public float knockbackStrength;
    }

    [SerializeField]
    private float _fireRate;
    private bool _firingAllowed = true;

    [SerializeField]
    private Transform _gunMuzzleTransform;
    [SerializeField]
    private IDirectionHandler _directionHandler;
    [SerializeField]
    private WeaponStruct[] _projectiles;


    [SerializeField]
    private GameObject _ShootFXPrefab;
    [SerializeField]
    private Transform _ShootFXSpawnPoint;

    private void Awake()
    {
        _directionHandler = GetComponent<IDirectionHandler>();
    }


    // Update is called once per frame
    void Update ()
    {
        if (InputController.staticInstance.GetKeyDown_Internal(InputKeyCodes.Shoot))
        {
            if(_firingAllowed == true)
            {
                Shoot(0);
            }
        }
    }


    private void Shoot(int projectileIndex)
    {
        LeftRight direction = _directionHandler.GetDirection();

        Vector2 spawnPosition = _ShootFXSpawnPoint.localPosition;


        GameObject currentProjectile = Instantiate(_projectiles[projectileIndex].weaponObject, _gunMuzzleTransform.position, Quaternion.identity,null);
        Rigidbody2D currentRB = currentProjectile.GetComponent<Rigidbody2D>();

        if (direction == LeftRight.Left)
        {
            spawnPosition.x *= -1f;
            currentProjectile.transform.localScale = new Vector3(-1f, 1f,1f);
        }

        currentProjectile.transform.position = (Vector2)transform.position + spawnPosition;

        currentRB.velocity =  (direction == LeftRight.Left ? Vector2.left : Vector2.right) * _projectiles[projectileIndex].fireStrength;

        SpawnShootFX(direction);


        _firingAllowed = false;
        Invoke("AllowFire", _fireRate);

    }

    private void AllowFire()
    {
        _firingAllowed = true;
    }

    private void SpawnShootFX(LeftRight direction)
    {

        Vector2 spawnPosition = _ShootFXSpawnPoint.localPosition;

        GameObject currentFX = Instantiate(_ShootFXPrefab, Vector2.zero, Quaternion.identity, transform);

        float xScale = 1f;
        if (direction == LeftRight.Left)
        {
            spawnPosition.x *= -1f;
            xScale = -1;
        }

        currentFX.transform.localScale = new Vector3(xScale, 1f, 1f);
        currentFX.transform.position = (Vector2)transform.position + spawnPosition;
    }
}
