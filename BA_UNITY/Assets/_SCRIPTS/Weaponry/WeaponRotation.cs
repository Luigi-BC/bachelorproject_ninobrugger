﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponRotation : MonoBehaviour, IDirectionHandler {


    [SerializeField]
    private LeftRight _direction;
    [SerializeField]
    private float _angle;
    [SerializeField]
    private Transform _rotationTransform;

    public LeftRight GetDirection()
    {
        return _direction;
    }

    // Update is called once per frame
    void Update ()
    {
        _angle = Vector2.SignedAngle(Vector2.left , (Vector2)Camera.main.WorldToScreenPoint(transform.position) - (Vector2)Input.mousePosition);
        _rotationTransform.rotation = Quaternion.Euler(0f, 0f, _angle);

        _direction = Mathf.Abs(_angle) > 90f ? LeftRight.Left : LeftRight.Right;


    }
}
