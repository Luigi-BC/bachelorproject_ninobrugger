﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{


    [SerializeField]
    private Button _hellButton;


    private void Start()
    {
        _hellButton.interactable = HellController.staticInstance.HasEnteredHell;
    }

    public void OnButtonPress_Start()
    {
        GameStateController.staticInstance.ChangeGameState(GameState.InLevel_Story);
    }

    public void OnButtonPress_Hell()
    {
        GameStateController.staticInstance.ChangeGameState(GameState.InLevel_Hell);
    }

    public void OnButtonPress_Quit()
    {
        GameStateController.staticInstance.ChangeGameState(GameState.Quitting);
    }
	
}
