﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleDisplay : MonoBehaviour {

    [SerializeField] private TitleStruct _titleBad;
    [SerializeField] private TitleStruct _titleGood;

    [System.Serializable]
    private struct TitleStruct
    {
        public Image _titleImage;
        public float _minShowTime;
        public float _maxShowTime;
    }


    private void Start()
    {
        StartCoroutine("ChangeTitles");
    }

    private IEnumerator ChangeTitles()
    {

        while (true)
        {
            //BAD
            _titleBad._titleImage.enabled = true;
            _titleGood._titleImage.enabled = false;
            yield return new WaitForSeconds(Random.Range(_titleBad._minShowTime, _titleBad._maxShowTime));

            //GOOD
            _titleGood._titleImage.enabled = true;
            _titleBad._titleImage.enabled = false;
            yield return new WaitForSeconds(Random.Range(_titleGood._minShowTime, _titleGood._maxShowTime));

        }

//        yield return null;
    }



}
