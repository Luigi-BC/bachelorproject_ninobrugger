﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

    [SerializeField]
    private GameObject _enemyPrefab;
    [SerializeField]
    private float _waitTimeAfterDeath;

    private EnemyBehavior _currentEnemyBehaviour;

    private void Start()
    {
        SpawnEnemy();
    }

    private async void OnEnemyDeath()
    {
        _currentEnemyBehaviour.EventDeath -= OnEnemyDeath;
        await InternalTime.WaitForInternalSeconds(_waitTimeAfterDeath);
        SpawnEnemy();
    }

    private void SpawnEnemy()
    {
        GameObject currentObject = Instantiate(_enemyPrefab);
        _currentEnemyBehaviour = currentObject.GetComponent<EnemyBehavior>();
        _currentEnemyBehaviour.EventDeath += OnEnemyDeath;
    }
}
