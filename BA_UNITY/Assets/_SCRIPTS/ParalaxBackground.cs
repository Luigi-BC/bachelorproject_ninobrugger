﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParalaxBackground : MonoBehaviour {

    [System.Serializable]
    private struct ParalaxLayer
    {
        public float movingSpeed;
        public Transform layerParent;
    }

    [SerializeField]
    private ParalaxLayer[] _paralaxLayers;

    [SerializeField]
    private Transform _camera;
	
	// Update is called once per frame
	void Update ()
    {
        for (int i = 0; i < _paralaxLayers.Length; i++)
        {
            Transform layerTransform = _paralaxLayers[i].layerParent;

            float xPos = Mathf.Lerp(0f, _camera.position.x, _paralaxLayers[i].movingSpeed); 
            layerTransform.position = new Vector3(xPos,layerTransform.position.y, 0f );
        }
	}
}
