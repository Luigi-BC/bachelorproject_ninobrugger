﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameStateController : MonoBehaviour
{
    [SerializeField]
    private GameState _currentGameState;
    public GameState CurrentGameState
    {
        get
        {
            return _currentGameState;
        }
        set
        {
            _currentGameState = value;
        }
    }

    public static GameStateController staticInstance;


    private void Awake()
    {
        if(staticInstance == null)
        {
            staticInstance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }


    }


    public void ChangeGameState(GameState newState)
    {
        if(CurrentGameState == newState)
        {
            return;
        }


        // CHECK OLD STATE
        switch (CurrentGameState)
        {
            case GameState.Initializing:

                break;
            case GameState.Menu:
                break;
            case GameState.InLevel_Story:
                SceneManager.UnloadSceneAsync(1);
                
                break;
            case GameState.InLevel_Hell:
                SceneManager.UnloadSceneAsync(2);
                break;
            case GameState.PauseMenu:

                break;
            case GameState.Quitting:
                break;
        }

        CurrentGameState = newState;

        switch (CurrentGameState)
        {
            case GameState.Initializing:
                break;
            case GameState.Menu:
                SceneManager.LoadScene(0);
                break;
            case GameState.InLevel_Story:
                SceneManager.LoadScene(1);
                InternalTime.timeScale = 1f;
                break;
            case GameState.InLevel_Hell:
                SceneManager.LoadScene(2);
                InternalTime.timeScale = 1f;
                break;
            case GameState.PauseMenu:
                break;
            case GameState.Quitting:
                Application.Quit();
                break;
        }
    }


    private List<string> _communicationEventCodes = new List<string>();
    
    public void AddExecutedEvent(string eventCode)
    {
        _communicationEventCodes.Add(eventCode);
    }

    public bool IsEventAlreadyExecuted(string eventCode)
    {
        return _communicationEventCodes.Contains(eventCode);
    }

}

public enum GameState
{
    Initializing,
    Menu,
    InLevel_Story,
    InLevel_Hell,
    PauseMenu,
    Quitting
}