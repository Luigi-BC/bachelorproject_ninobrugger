﻿using System.Collections;
using System.Collections.Generic;
using UnityAsync;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HellTrigger : CommunicationEventBase
{

   

    public async void Event_EnterHell()
    {
        HellController.staticInstance.HasEnteredHell = true;

        InternalTime.FreezeTime(false);
        ShowMessage(0);
        await InternalTime.WaitForInternalSeconds(6f);
        ShowMessage(1);
        await InternalTime.WaitForInternalSeconds(3.5f);
        ShowMessage(2);
        await InternalTime.WaitForInternalSeconds(5f);
        InternalTime.UnfreezeTime(false);
        GameStateController.staticInstance.ChangeGameState(GameState.InLevel_Hell);
        await SceneManager.LoadSceneAsync(2);

    }

}
