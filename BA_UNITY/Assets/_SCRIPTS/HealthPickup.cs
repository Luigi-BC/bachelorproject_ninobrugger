﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickup : MonoBehaviour
{

    [SerializeField]
    public float _healthValue;

    [SerializeField]
    private LayerMask _playerLayerMask;

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if(collision.gameObject.layer != 8 )
        {
            return;
        }

        PlayerHealth.staticInstance.Health += _healthValue;
        Destroy(gameObject);
    }
}
