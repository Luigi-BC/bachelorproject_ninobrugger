﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StorySetupEventHandler : CommunicationEventBase{




    public void Event_StorySetup()
    {
        if(CommunicationLevels.staticInstance._skipIntro == false)
        {
            StorySetup();
        }
        else
        {
            CommunicationLevels.staticInstance.CurrentLevel++;
        }
    }

    private async void StorySetup()
    {
        await InternalTime.WaitForInternalSeconds(1.5f);
        ShowMessage(0);
        await InternalTime.WaitForInternalSeconds(3f);
        ShowMessage(1);
        await InternalTime.WaitForInternalSeconds(6f);
        ShowMessage(2);
        await InternalTime.WaitForInternalSeconds(6f);
        CommunicationLevels.staticInstance.CurrentLevel++;
    }
}
