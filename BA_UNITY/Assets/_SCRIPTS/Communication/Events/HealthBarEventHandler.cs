﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class HealthBarEventHandler : CommunicationEventBase
{

    [SerializeField]
    private HealthBar _healthBar;
    /// <summary>
    /// Healthbar of Player
    /// </summary>
    [SerializeField]
    private Canvas _healthBarCanvas;

    private string eventCode = "HealthBar";
    private void Start()
    {

        if(GameStateController.staticInstance.CurrentGameState == GameState.InLevel_Hell)
        {
            ChangeVisibility(true);
            return;
        }

        if (GameStateController.staticInstance.IsEventAlreadyExecuted(eventCode))
        {
            ChangeVisibility(true);
            return;
        }


        ChangeVisibility(false);
    }

    public void Event_ActivateHealthBar()
    {
        if (GameStateController.staticInstance.IsEventAlreadyExecuted(eventCode))
        {
            ChangeVisibility(true);
            return;
        }

        ActivateHealthBar();
    }

    private async void ActivateHealthBar()
    {
        InternalTime.FreezeTime(false);
        
        ShowMessage(0);
        await InternalTime.WaitForInternalSeconds(2f);

        ShowMessage(1);
        await InternalTime.WaitForInternalSeconds(4f);
        ChangeVisibility(true);
        ShowMessage(2);

        InternalTime.UnfreezeTime(false);
        GameStateController.staticInstance.AddExecutedEvent(eventCode);
    }

    public void ChangeVisibility(bool state)
    {
        _healthBarCanvas.enabled = state;
    }

}