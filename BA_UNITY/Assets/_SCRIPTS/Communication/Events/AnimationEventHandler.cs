﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEventHandler : CommunicationEventBase{


    [SerializeField]
    private bool _animationsEnabled;
    public bool AnimationsEnabled
    {
        get { return _animationsEnabled; }
        set
        {
            EventAnimationStateChanged(_animationsEnabled = value);
        }
    }
    public Action<bool> EventAnimationStateChanged = delegate { };

    public static AnimationEventHandler staticInstance;

    private string eventCode = "Animations";

    private void Awake()
    {

        if(staticInstance == null)
        {
            staticInstance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        if (GameStateController.staticInstance.IsEventAlreadyExecuted(eventCode))
        {
            AnimationsEnabled = true;
        }
    }

    


    public void Event_EnableAnimations()
    {
        if (GameStateController.staticInstance.IsEventAlreadyExecuted(eventCode))
        {
            return;
        }

        EnableAnimationEvent();
    }


    private async void EnableAnimationEvent()
    {
        InternalTime.FreezeTime(false);
        ShowMessage(0);
        await InternalTime.WaitForInternalSeconds(3f);
        ShowMessage(1);
        await InternalTime.WaitForInternalSeconds(3f);
        InternalTime.UnfreezeTime(false);
        AnimationsEnabled = true;

        GameStateController.staticInstance.AddExecutedEvent(eventCode);
    }
}
