﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEventHandler : CommunicationEventBase
{

    public void Event_PlayerMovement()
    {
        if(CommunicationLevels.staticInstance._skipIntro == false)
        {
            MovementMessages();
        }
        else
        {
            InputController.staticInstance.IgnoreAllInput = false;
            CommunicationLevels.staticInstance.CurrentLevel++;

        }
    }


    private void Start()
    {
        if(GameStateController.staticInstance.CurrentGameState == GameState.InLevel_Hell)
        {
            Destroy(this);
            return;
        }
        InputController.staticInstance.IgnoreAllInput = true;
    }

    private async void MovementMessages()
    {

        ShowMessage(0);
        await InternalTime.WaitForInternalSeconds(4f);
        ShowMessage(1);
        await InternalTime.WaitForInternalSeconds(3f);
        ShowMessage(2);
        await InternalTime.WaitForInternalSeconds(3f);
        ShowMessage(3);
        InputController.staticInstance.IgnoreAllInput = false;

        GameStateController.staticInstance.AddExecutedEvent(CommunicationLevels.staticInstance.startingEventCode);

        CommunicationLevels.staticInstance.CurrentLevel++;
    }

}
