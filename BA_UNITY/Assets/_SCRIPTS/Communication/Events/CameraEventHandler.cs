﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraEventHandler : CommunicationEventBase {


    private Camera _camera;
    [SerializeField]
    private LayerMask _startingLM;
    [SerializeField]
    private LayerMask _backgroundLM;
    [SerializeField]
    private LayerMask _environmentLM;
    [SerializeField]
    private LayerMask _gameLM;
    [SerializeField]
    private LayerMask _colliderMask;


    private void Start()
    {
        _camera = GetComponent<Camera>();
        if (GameStateController.staticInstance.CurrentGameState != GameState.InLevel_Hell)
        {
            _camera.cullingMask = _startingLM;
        }
    }

    public void Event_FirstMessage()
    {
        if(CommunicationLevels.staticInstance._skipIntro == false)
        {
            FirstMessage();
        }
        else
        {
            _camera.cullingMask = _gameLM;
            CommunicationLevels.staticInstance.CurrentLevel++;
        }
    }

    private async void FirstMessage()
    {
        await InternalTime.WaitForInternalSeconds(4f);

        ShowMessage(0);
        await InternalTime.WaitForInternalSeconds(10f);

        ShowMessage(1);
        await InternalTime.WaitForInternalSeconds(5f);

        ShowMessage(2);
        _camera.cullingMask = _backgroundLM;
        await InternalTime.WaitForInternalSeconds(5f);

        ShowMessage(3);
        _camera.cullingMask = _environmentLM;
        await InternalTime.WaitForInternalSeconds(5f);

        ShowMessage(4);
        _camera.cullingMask = _gameLM;
        await InternalTime.WaitForInternalSeconds(5f);

        CommunicationLevels.staticInstance.CurrentLevel++;
    }

    public void ChangeColliderView(bool state)
    {
        if (state == true)
        {
            _camera.cullingMask = _colliderMask;
        }
        else
        {
            _camera.cullingMask = _gameLM;
        }
    }
}
