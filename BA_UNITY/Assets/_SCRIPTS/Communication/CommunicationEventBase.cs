﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using UnityAsync;
using UnityEngine;

public class CommunicationEventBase : MonoBehaviour {

    [SerializeField]
    protected UIMessage[] _messages;
    private bool _isActive;

    protected void ShowMessage(int index)
    {
        if(index >= _messages.Length)
        {
            Debug.LogError("ArrayIndexOutOfRange for message on " + name);
            return;
        }
        UIMessage msg = _messages[index];
        UIMessageBox.ShowMessage(msg.messageText, msg.messageShowTime);
    }

    
    
}


[Serializable]
public struct UIMessage
{
    [TextArea]
    public string messageText;
    public float messageShowTime;
}