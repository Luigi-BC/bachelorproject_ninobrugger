﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpEventHandler : CommunicationEventBase
{

    [SerializeField]
    private float _timeBeforeHint;


    [SerializeField]
    private bool _isChecking;

    public void Event_MovementAllowed()
    {
        ShowHint();
        _isChecking = true;
    }

    private void Update()
    {
        if (_isChecking)
        {
            if (InputController.staticInstance.GetKeyDown_Internal(InputKeyCodes.Jump))
            {
                _isChecking = false;
                enabled = false;
            }
        }
    }

    private async void ShowHint()
    {
        await InternalTime.WaitForInternalSeconds(_timeBeforeHint);
        if(_isChecking == true)
        {
            ShowMessage(0);
            enabled = false;
        }

    }
}
