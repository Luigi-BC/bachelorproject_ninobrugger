﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CameraReplacement : MonoBehaviour {

    [SerializeField]
    private Material _mat;

    [SerializeField]
    [Range(0.01f, 1f)]
    private float _resolutionScale = 1;

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        int width = (int) (source.width * _resolutionScale);
        int height = (int) (source.height * _resolutionScale);

        RenderTexture rt = RenderTexture.GetTemporary(width, height);
        Graphics.Blit(source, rt);

        RenderTexture rt2 = RenderTexture.GetTemporary(width, height);
        Graphics.Blit(rt, rt2, _mat);

        Graphics.Blit(rt2, destination);

        RenderTexture.ReleaseTemporary(rt);
        RenderTexture.ReleaseTemporary(rt2);
    }

}
