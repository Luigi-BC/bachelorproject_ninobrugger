﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CommunicationLevels : MonoBehaviour {

    public static CommunicationLevels staticInstance;

    public bool _skipIntro;


    private void Awake()
    {
        if(staticInstance == null)
        {
            staticInstance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    
    [SerializeField]
    private int _currentLevel;
    public int CurrentLevel
    {
        get { return _currentLevel; }
        set
        {
            EventCommunicationLevelChanged(_currentLevel, _currentLevel = value);
            _communicationEvents[value]._event.Invoke();
        }
    }

    [SerializeField]
    private List<CommunicationEvent> _communicationEvents = new List<CommunicationEvent>();


    public Action<int, int> EventCommunicationLevelChanged = delegate { };


    public string startingEventCode = "startingEvent";

    private void Start()
    {
        if (GameStateController.staticInstance.IsEventAlreadyExecuted(startingEventCode))
        {
            _skipIntro = true;
        }

        CurrentLevel = 0;
    }

}
