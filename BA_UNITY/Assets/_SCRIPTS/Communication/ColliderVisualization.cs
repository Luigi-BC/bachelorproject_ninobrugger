﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ColliderVisualization : MonoBehaviour{



    [SerializeField]
    private CameraEventHandler _camera;

    [SerializeField]
    private GameObject _colliderViewPrefab;

    [SerializeField]
    private bool _colliderViewActive;
    public bool ColliderViewActive
    {
        get { return _colliderViewActive; }
        set
        {
            if(value == _colliderViewActive)
            {
                return;
            }

            _colliderViewActive = value;

            CheckTransforms();

        }
    }


    public void CheckTransforms()
    {

        Transform[] allTransforms = FindObjectsOfType<Transform>();

        if(ColliderViewActive == true)
        {
            foreach(Transform t in allTransforms)
            {
                ActivateColliderView(t);
            }
        }
        else
        {
            foreach (Transform t in allTransforms)
            {
                DeactivateColliderView(t);
            }
        }

        _camera.ChangeColliderView(ColliderViewActive);
    }

    private void ActivateColliderView(Transform _transform)
    {

        BoxCollider2D collider = _transform.GetComponent<BoxCollider2D>();
        
        if(collider != null && !collider.isTrigger)
        {
            GameObject colliderView = Instantiate(_colliderViewPrefab, _transform.position, Quaternion.identity, _transform);

            colliderView.transform.position = collider.bounds.center;
            colliderView.transform.localScale = new Vector3(collider.bounds.extents.x * 2f, collider.bounds.extents.y * 2f, 1f);

        }

        ChangeSpriteState(_transform, false);

    }


    private void ChangeSpriteState(Transform _transform, bool state)
    {
        SpriteRenderer spriteRenderer = _transform.GetComponent<SpriteRenderer>();
        TilemapRenderer tilemapRenderer = _transform.GetComponent<TilemapRenderer>();

        if (spriteRenderer != null)
        {
            spriteRenderer.enabled = state;
        }

        if (tilemapRenderer != null)
        {
            tilemapRenderer.enabled = state;
        }
    }

    private void DeactivateColliderView(Transform _transform)
    {

        if(_transform.tag == "ColliderView")
        {
            Destroy(_transform.gameObject);
        }

        ChangeSpriteState(_transform, true);

    }

}
