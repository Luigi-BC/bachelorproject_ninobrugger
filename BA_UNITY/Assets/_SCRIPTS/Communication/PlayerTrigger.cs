﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider))]
public class PlayerTrigger : MonoBehaviour {


    [SerializeField]
    private UnityEvent _event;
    [SerializeField]
    private bool _fireOnce = true;

    [SerializeField]
    protected Action EventHellEntered = delegate { };

    private bool _alreadyFired;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.layer == 8)
        {
            if (_fireOnce == true && _alreadyFired == true) { return; }

            if(_event != null)
            {
                _event.Invoke();
            }
            EventHellEntered();

            _alreadyFired = true;
        }
    }

}
