﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HellController : MonoBehaviour {

    private SaveData _saveData;

    public static HellController staticInstance;

    public bool HasEnteredHell
    {
        get
        {
            return _saveData.hellEntered;
        }
        set
        {
            _saveData.hellEntered = value;
            SaveLoad.SaveData<SaveData>(_saveData, "TBG_Save.tbg");
        }
    }


    private void Awake()
    {

        if(staticInstance == null)
        {
            staticInstance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        _saveData = SaveLoad.LoadData<SaveData>("TBG_Save.tbg");
        if(_saveData == null)
        {
            _saveData = new SaveData();
        }
    }

}
