﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletJoke : CommunicationEventBase {


    private string eventCode = "BulletJoke";

	public async void Event_BulletJoke()
    {

        if (GameStateController.staticInstance.IsEventAlreadyExecuted(eventCode))
        {
            return;
        }

        ShowMessage(0);
        await InternalTime.WaitForInternalSeconds(3.5f);
        ShowMessage(1);
        await InternalTime.WaitForInternalSeconds(2.5f);
        ShowMessage(2);
        await InternalTime.WaitForInternalSeconds(3.5f);
        ShowMessage(3);

        GameStateController.staticInstance.AddExecutedEvent(eventCode);

    }
}
