﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CommunicationMenu : MonoBehaviour
{

    

    [SerializeField]
    private Material _cameraMaterial;
    public void ChangeGrayScales(bool state)
    {
        _cameraMaterial.SetFloat("_GrayScale", state ? 1f : 0f);
    }


    [SerializeField]
    private ColliderVisualization _colliderVisualization;
    public void ChangeCollisionLayerVisibility(bool state)
    {
        _colliderVisualization.ColliderViewActive = state;
    }


    [SerializeField]
    private AnimationEventHandler _animationEvents;
    public void ChangeAnimationState(bool state)
    {
        _animationEvents.AnimationsEnabled = !state;
    }


    [SerializeField]
    private HealthBarEventHandler _healthBarEvents;
    public void ChangeHealthBarVisibility(bool state)
    {
        _healthBarEvents.ChangeVisibility(!state);        
    }


    [SerializeField]
    private Canvas _pauseMenuCanvas;
    [SerializeField]
    private Toggle _switchUIToggle;
    public async void ChangeUIVisibility(bool state)
    {
        if(state == true)
        {
            _pauseMenuCanvas.enabled = false;
            _healthBarEvents.ChangeVisibility(false);

            InternalTime.timeScale = 1f;

            await InternalTime.WaitForInternalSeconds(0.5f);
            UIMessageBox.ShowMessage("You didn't think that one through, did you?", 2f);
            await InternalTime.WaitForInternalSeconds(2f);

            InternalTime.timeScale = 0f;
            _switchUIToggle.isOn = false;
            _pauseMenuCanvas.enabled = true;
            _healthBarEvents.ChangeVisibility(true);

        }
    }


    [SerializeField]
    private PauseMessage _pauseMessage;
    public async void IgnoreInput(bool state)
    {


        InputController.staticInstance._ignoreAllInput_override = state;
        InputController.staticInstance.IgnoreAllInput= state;


        if (state != true) { return;  }

        PauseMessageResult result = await _pauseMessage.OpenQuestion("Are you sure you want to ignore ALL input?");


        if(result == PauseMessageResult.Yes)
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
        else
        {
            await _pauseMessage.OpenMessage("Good. You can keep your cursor then.", 2.5f);
        }
    }


    [SerializeField] private Camera _camera;
    public void ChangeEnvironmentVisibility(bool state)
    {
        if(state == true)
        {
            //Turn OFF Environment
            _camera.cullingMask &= ~(1 << 9);
        }
        else
        {
            //Turn ON Environment
            _camera.cullingMask = _camera.cullingMask | (1 << 9);
        }
    }

    public void ChangeBackgroundVisibility(bool state)
    {
        if(state == true)
        {
            //Turn OFF Background
            _camera.cullingMask &= ~(1 << 14);
        }
        else
        {
            //Turn ON Background
            _camera.cullingMask = _camera.cullingMask | (1 << 14);
        }
    }

    public void ChangeCameraClearFlags(bool state)
    {
        if(state == true)
        {
            //Dont Clear
            _camera.clearFlags = CameraClearFlags.Nothing;
        }
        else
        {
            //Normal
            _camera.clearFlags = CameraClearFlags.SolidColor;
            _camera.backgroundColor = new Color(0f, 0f, 0f,1f);
        }
    }

}
