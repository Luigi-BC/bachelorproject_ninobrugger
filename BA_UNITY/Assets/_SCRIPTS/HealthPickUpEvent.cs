﻿using System.Collections;
using System.Collections.Generic;
using UnityAsync;
using UnityEngine;

public class HealthPickUpEvent : CommunicationEventBase
{

    private void Start()
    {
        if (GameStateController.staticInstance.CurrentGameState == GameState.InLevel_Hell)
        {
            Destroy(this);
            return;
        }
    }

    public void Event_HealthPickupPickedUp()
    {
        ShowMessage(0);
    }
}
