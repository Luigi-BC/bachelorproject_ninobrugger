﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHealth
{

    float Health { get; set; }

    Action EventDamageTaken { get; set; }

    void OnHit(float damage);

}
