﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ViewDetection {

    [SerializeField]
    private LayerMask _targetLayer;
    [SerializeField]
    public float _detectionRadius;
    [SerializeField]
    private Transform _startingTransform;
    public Vector2 _lookDirection;

    

    public ViewDetection(Transform startingTransform, LayerMask targetLayer)
    {
        _startingTransform = startingTransform;
        _targetLayer = targetLayer;
    }



    public bool CheckSight()
    {
        return Physics2D.Raycast(_startingTransform.position, _lookDirection, _detectionRadius, _targetLayer);
    }

    public bool CheckDistanceToTarget(Transform target)
    {
        return Vector2.Distance(_startingTransform.position, target.position) < _detectionRadius;
    }



}
