﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDirectionHandler  {


    LeftRight GetDirection();


}

public enum LeftRight { Left, Right }