﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlippingHandler : MonoBehaviour {

    [SerializeField]
    private IDirectionHandler _directionHandler;

    [SerializeField]
    private SpriteRenderer[] _spritesToFlip;


	// Use this for initialization
	private void Awake ()
    {
        _directionHandler = GetComponent<IDirectionHandler>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		for(int i = 0; i < _spritesToFlip.Length; i++)
        {
            _spritesToFlip[i].flipX = _directionHandler.GetDirection() == LeftRight.Left;
        }
	}
}
