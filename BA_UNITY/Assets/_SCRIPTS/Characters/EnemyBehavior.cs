﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyBehavior : MonoBehaviour, IDirectionHandler
{

    public Action EventDeath = delegate { };



    [SerializeField]
    private Transform[] _waypoints;
    private Vector3[] _waypointPositions;
    [SerializeField]
    private int _targetWayPoint;
    private LeftRight _targetDirection;
    [SerializeField]
    private Transform _enemyTransform;

    [SerializeField]
    private bool _useOnlyPatrolling;
    [SerializeField]
    private ViewDetection _viewDetection;

    public Action EventAttack = delegate { };
    [SerializeField]
    public float _movementSpeed;
    [SerializeField]
    private float _stopAtCheckPointTime;
    [SerializeField]
    private float _stopAfterAttackTime;
    [SerializeField]
    private bool _movementStopped;
    [SerializeField]
    private bool _attackAllowed;

    [SerializeField]
    private EnemyState _currentEnemyState;

    public void Death()
    {
        EventDeath();
        Destroy(this.gameObject);
    }

    private EnemyState CurrentEnemyState
    {
        get { return _currentEnemyState; }
        set
        {
            if(_currentEnemyState != value)
            {
                _currentEnemyState = value;

                switch (_currentEnemyState)
                {
                    case EnemyState.Patrolling:
                        break;
                    case EnemyState.Stopped:
                        break;
                    case EnemyState.Alarmed:
                        break;
                }
            }
        }
    }

    public float _currentSpeed;


    
    // Use this for initialization
    void Start ()
    {
        _waypointPositions = new Vector3[_waypoints.Length];
        for (int i = 0; i < _waypoints.Length; i++)
        {
            _waypointPositions[i] = _waypoints[i].position;
        }
    }

    

    void FixedUpdate ()
    {

        _viewDetection._lookDirection = _targetDirection == LeftRight.Left ? Vector2.left : Vector2.right;

        if (_useOnlyPatrolling == true && _movementStopped == false)
        {
            WalkTowardsPoint(_waypointPositions[_targetWayPoint]);
            _currentSpeed = 1f;
            return;
        }
        _currentSpeed = 0f;

        if (_attackAllowed == true)
        {
            TryAttack();
        }

        if (_movementStopped == false && CurrentEnemyState != EnemyState.Stopped)
        {
            WalkTowardsPoint(_waypointPositions[_targetWayPoint]);
            _currentSpeed= 1f;
        }

    }

    private void TryAttack()
    {
        if (_viewDetection.CheckDistanceToTarget(Player.staticInstance.playerTransform))
        {
            if (_viewDetection.CheckSight())
            {
                // Is seeing target
                CurrentEnemyState = EnemyState.Alarmed;
                Attack();
            }
            else
            {
                //ise not seeing target
                CurrentEnemyState = EnemyState.Patrolling;
            }
        }
    }

    private void Attack()
    {
        EventAttack();
        _movementStopped = true;
        CurrentEnemyState = EnemyState.Stopped;
        _attackAllowed = false;
        Invoke("StartMovement", _stopAfterAttackTime);
    }


    [SerializeField]
    private Transform _hitCollider;
    public void ActivateHitCollider()
    {
        _hitCollider.gameObject.SetActive(true);
        if(_targetDirection == LeftRight.Right)
        {
            _hitCollider.localScale = Vector3.one;
        }
        else
        {
            _hitCollider.localScale = new Vector3(-1f, 1f, 1f);
        }
    }
    public void DeactivateHitCollider()
    {
        _hitCollider.gameObject.SetActive(false);
    }

    private void WalkTowardsPoint(Vector3 targetPosition)
    {
        Vector3 differenceVector = targetPosition - _enemyTransform.position;

        if(differenceVector.x > 0)
        {
            //Go right
            _enemyTransform.position += Vector3.right * _movementSpeed * Time.fixedDeltaTime;
            _targetDirection = LeftRight.Right;
        }
        else
        {
            //Go left
            _enemyTransform.position += Vector3.left * _movementSpeed * Time.fixedDeltaTime;
            _targetDirection = LeftRight.Left;
        }

        if (PointReachedCheck(targetPosition))
        {
            StopMovement();
            ChangeTarget();
            _enemyTransform.position = new Vector3(targetPosition.x, _enemyTransform.position.y, _enemyTransform.position.z);
        }
    }

    private void StopMovement()
    {
        _movementStopped = true;
        CurrentEnemyState = EnemyState.Stopped;
        Invoke("StartMovement", _stopAtCheckPointTime);
    }

    private void StartMovement()
    {
        _movementStopped = false;
        CurrentEnemyState = EnemyState.Patrolling;
        _attackAllowed = true;
    }

    private void ChangeTarget()
    {
        _targetWayPoint++;
        if(_targetWayPoint >= _waypointPositions.Length)
        {
            _targetWayPoint = 0;
        }
    }

    private bool PointReachedCheck(Vector3 targetPosition)
    {
        switch (_targetDirection)
        {
            case LeftRight.Left:
                return targetPosition.x > _enemyTransform.position.x;
            case LeftRight.Right:
                return targetPosition.x < _enemyTransform.position.x;
        }
        return false;
    }

    private void OnDrawGizmos()
    {
        for (int i = 0; i < _waypoints.Length; i++)
        {
            Gizmos.DrawWireSphere(_waypoints[i].position, 0.2f);
            if(i != 0)
            {
                Gizmos.DrawLine(_waypoints[i].position, _waypoints[i - 1].position);
            }
        }
    }

    public LeftRight GetDirection()
    {
        return _targetDirection;
    }
}


public enum EnemyState
{
    Patrolling,
    Stopped,
    Alarmed
}
