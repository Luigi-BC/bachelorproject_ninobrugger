﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour, IHealth {




    [SerializeField]
    private float _health;
    public float Health
    {
        get {  return _health; }
        set
        {
            if(value <= 0)
            {
                Death();
            }

            if(value < _health)
            {
                EventDamageTaken();
            }

            _health = value;
        }
    }

    private Action _eventDamageTaken = delegate { };

    public Action EventDamageTaken
    {
        get
        {
            return _eventDamageTaken;
        }

        set
        {
            _eventDamageTaken = value;
        }
    }

    //public Action EventDamageTaken = delegate { };

    [SerializeField]
    private EnemyBehavior _enemyContainer;

    private void Death()
    {
        _enemyContainer.Death();
    }

    public void OnHit(float damage)
    {
        Health -= damage;
    }
}
