﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(IHealth))]
public class HitIndicator : MonoBehaviour
{

    [SerializeField]
    private SpriteRenderer _sprite;
    private IHealth _iHealth;

    // Use this for initialization
    private void Awake()
    {
        _iHealth = GetComponent<IHealth>();
        _iHealth.EventDamageTaken += OnHit;
    }

    private void OnDestroy()
    {
        _iHealth.EventDamageTaken -= OnHit;
    }

    private void OnHit()
    {
        StopCoroutine("HitAnimation");
        StartCoroutine("HitAnimation");
    }

    public IEnumerator HitAnimation()
    {
        _sprite.color = Color.red;
        yield return new WaitForSeconds(0.1f);
        _sprite.color = Color.white;
        yield return new WaitForSeconds(0.1f);
        _sprite.color = Color.red;
        yield return new WaitForSeconds(0.1f);
        _sprite.color = Color.white;
        yield return null;

    }

}