﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public static Player staticInstance;

    [HideInInspector]
    public Transform playerTransform;

	private void Awake()
    {
		if(staticInstance == null)
        {
            staticInstance = this;
        }
        playerTransform = transform;
    }
}
