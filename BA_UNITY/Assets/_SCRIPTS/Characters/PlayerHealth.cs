﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour, IHealth {

    public static PlayerHealth staticInstance;

    public Action<float> EventHealthChanged = delegate { };

    public float _startingHealth;

    private float _health; 
    public float Health
    {
        get
        {
            return _health;
        }

        set
        {

            if (value < _health)
            {
                EventDamageTaken();
            }

            _health = Mathf.Clamp(value, 0f, _startingHealth);
            EventHealthChanged(_health);
            if (value <= 0)
            {
                Death();
            }
            
        }
    }

    private Action _eventDamageTaken = delegate { };

    public Action EventDamageTaken
    {
        get
        {
            return _eventDamageTaken;
        }

        set
        {
            _eventDamageTaken = value;
        }
    }

    private void Awake()
    {
        if(staticInstance== null)
        {
            staticInstance = this;
            InitializeValues();
        }
    }

    private void InitializeValues()
    {
        Health = _startingHealth;
    }

    private void Death()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }   

    public void OnHit(float damage)
    {

        Health -= damage;

    }
}
