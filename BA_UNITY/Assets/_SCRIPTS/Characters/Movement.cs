﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Movement : MonoBehaviour, IDirectionHandler, IAnimationValues {

    [SerializeField]
    private float _movementSpeed;
    [SerializeField]
    [Range(0,1)]
    private float _movementLerpingOnGround;

    [SerializeField]
    [Range(0, 1)]
    private float _movementLerpingInAir;

    private float _horizontalRawInput;
    [SerializeField]
    private float _jumpForce;
    [SerializeField]
    private float _gravityMultiplierOnFall;
    private float _startingGravity;
    [SerializeField]
    private bool _isGrounded;
    [SerializeField]
    private Collider2D _jumpCollider;
    private Rigidbody2D _rigidbody;
    [SerializeField]
    private LayerMask _environmentLayer;
    private LeftRight _currentDirection;

    private float _currentSpeed;

    private void Awake()
    {

        _rigidbody = GetComponent<Rigidbody2D>();
        _startingGravity = _rigidbody.gravityScale;
        _environmentLayer = 1 << 9;
    }
	
	// Update is called once per frame
	private void Update ()
    {
        _isGrounded = CheckGrounded();


        _horizontalRawInput= GetHorizontalInput();
        if (GetJumpInput() && _isGrounded)
        {
            Jump();
        }
	}

    private bool CheckGrounded()
    {
        return _jumpCollider.IsTouchingLayers(_environmentLayer);
    }

    private bool GetJumpInput()
    {
        return InputController.staticInstance.GetKeyDown_Internal(InputKeyCodes.Jump);
    }

    private void Jump()
    {
        _rigidbody.velocity = new Vector2(_rigidbody.velocity.x, _jumpForce);
    }

    private float GetHorizontalInput()
    {
        float horizontalInput = InputController.staticInstance.GetAxis_Internal(InputKeyCodes.WalkLeft, InputKeyCodes.WalkRight);
        if(horizontalInput != 0)
        {
            _currentDirection = horizontalInput > 0f ? LeftRight.Right : LeftRight.Left;
        }
        return horizontalInput;
    }

    private void FixedUpdate()
    {


        //This code gets executed every frame (Physics Engine)
        //The desired speed is the input (-1 to 1, 0 if no buttons pressed) times the speed and the time passed since the last frame
        float desiredSpeed = _horizontalRawInput * _movementSpeed * Time.fixedDeltaTime;
        

        //if the player is grounded
        if (_isGrounded)
        {
            //let him move normally
            _currentSpeed = Mathf.Lerp(_currentSpeed, desiredSpeed, _movementLerpingOnGround);
        }
        else
        {
            //if he's in the air make him move slower
            _currentSpeed = Mathf.Lerp(_currentSpeed, desiredSpeed, _movementLerpingInAir);
        }
        
        //apply the speed
        _rigidbody.velocity = new Vector2(_currentSpeed, _rigidbody.velocity.y);
        
        //if the player is falling
        if(_rigidbody.velocity.y < 0)
        {
            // make him heavier (up the gravity)
            _rigidbody.gravityScale = _startingGravity * _gravityMultiplierOnFall;
        }
        else
        {
            //else have him use the normal gravity
            _rigidbody.gravityScale = _startingGravity;
        }



    }

    public LeftRight GetDirection()
    {
        return _currentDirection;
    }

    public float GetSpeed()
    {
        return Mathf.Abs(_rigidbody.velocity.x);
    }

    public bool IsGrounded()
    {
        return _isGrounded;
    }

    public float GetYSpeed()
    {
        return _rigidbody.velocity.y;
    }
}
