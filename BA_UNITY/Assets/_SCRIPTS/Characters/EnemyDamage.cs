﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class EnemyDamage : MonoBehaviour {


    [SerializeField]
    private float _damage;

    [SerializeField]
    private float _cooldown;

    [SerializeField]
    private bool _isTouchingPlayer;
    private bool IsTouchingPlayer
    {
        get { return _isTouchingPlayer; }
        set
        {
            if (_isTouchingPlayer != value)
            {
                _isTouchingPlayer = value;
                if(value == true)
                {
                    HitPlayer();
                }
            }
        }
    }

    private void HitPlayer()
    {

        if(Time.timeScale == 0)
        {
            return;
        }

        if (IsTouchingPlayer== true)
        {
            PlayerHealth.staticInstance.OnHit(_damage);
            if (_cooldown > 0f)
            {
                Invoke("HitPlayer", _cooldown);
            }
        }
    }

    private Collider2D _collider;


	// Use this for initialization
	private void Awake ()
    {
        _collider = GetComponent<Collider2D>();
    }


    private void FixedUpdate()
    {
        
        IsTouchingPlayer = _collider.IsTouchingLayers(1 << 13);
    }
}
