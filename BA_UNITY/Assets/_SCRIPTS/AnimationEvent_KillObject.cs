﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEvent_KillObject : MonoBehaviour {

    private async void Start()
    {
        if(AnimationEventHandler.staticInstance.AnimationsEnabled == false)
        {
            await InternalTime.WaitForInternalSeconds(0.4f);
            KillObject();
        }
    }

    public void KillObject()
    {
        Destroy(transform.parent.gameObject);
    }

}
