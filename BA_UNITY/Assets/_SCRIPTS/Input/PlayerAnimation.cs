﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(IAnimationValues))]
public class PlayerAnimation : MonoBehaviour {

    private IAnimationValues _animationHandler;
    [SerializeField]
    private Animator _animator;



    // Use this for initialization
    void Awake ()
    {
        _animationHandler = GetComponent<IAnimationValues>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        _animator.SetFloat("Speed", _animationHandler.GetSpeed());
        _animator.SetBool("Grounded", _animationHandler.IsGrounded());
        _animator.SetFloat("YSpeed", _animationHandler.GetYSpeed());

    }
}
