﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAnimationValues
{
    
    float GetSpeed();
    bool IsGrounded();
    float GetYSpeed();
}
