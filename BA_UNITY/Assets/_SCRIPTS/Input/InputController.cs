﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    [SerializeField]
    private InputModel _model;

    
    public bool _ignoreAllInput = true;
    public bool IgnoreAllInput
    {
        get { return _ignoreAllInput; }
        set
        {
            if(_ignoreAllInput_override == true)
            {
                _ignoreAllInput = true;
            }
            else
            {
                _ignoreAllInput = value;
            }
        }
    }
    public bool _ignoreAllInput_override;

    public static InputController staticInstance;

    private void Awake()
    {
        if(staticInstance == null)
        {
            staticInstance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }


        //Load Input Model
        //TO COMPLETE (No Loading yet)
        _model = new InputModel();
    }


    public bool GetKeyDown_Internal(InputKeyCodes keyCode)
    {
        if (IgnoreAllInput)
        {
            return false;
        }

        return Input.GetKeyDown(_model._internalKeyCodes[(int)keyCode]);
    }

    public bool GetButtonDown_Internal(InputMouseCodes mouseCode)
    {
        if (IgnoreAllInput)
        {
            return false;
        }

        return Input.GetMouseButtonDown((int)mouseCode);
    }


    public float GetAxis_Internal(InputKeyCodes negativeKey, InputKeyCodes positiveKey)
    {
        if (IgnoreAllInput)
        {
            return 0f;
        }


        float axisValue = 0f;

        if (Input.GetKey(_model._internalKeyCodes[(int)negativeKey]))
        {
            axisValue = -1f;
        }
        if (Input.GetKey(_model._internalKeyCodes[(int)positiveKey]))
        {
            axisValue = 1f;
        }
        return axisValue;

    }


}
