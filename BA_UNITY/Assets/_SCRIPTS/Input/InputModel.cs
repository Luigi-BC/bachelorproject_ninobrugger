﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class InputModel
{

    public KeyCode[] _internalKeyCodes;
    public int[] _internalMouseCodes;

    public InputModel()
    {
        _internalKeyCodes = new KeyCode[Enum.GetNames(typeof(InputKeyCodes)).Length];
        _internalKeyCodes[0] = KeyCode.LeftArrow;
        _internalKeyCodes[1] = KeyCode.RightArrow;
        _internalKeyCodes[2] = KeyCode.LeftControl;
        _internalKeyCodes[3] = KeyCode.LeftAlt;

        _internalMouseCodes = new int[Enum.GetNames(typeof(InputMouseCodes)).Length];
        _internalMouseCodes[0] = 0;
    }
}

[SerializeField]
public enum InputMouseCodes { Shoot }

[Serializable]
public enum InputKeyCodes { WalkLeft, WalkRight, Jump, Shoot}
