﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

public class SaveLoadController
{
    private static string savePath = Application.persistentDataPath + "/";

    public static void Save<T>(T saveData, string dataName)
    {
        FileStream fs = new FileStream(savePath + dataName, FileMode.OpenOrCreate);

        try
        {
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs, saveData);
        }
        catch (SerializationException e)
        {
            Debug.LogError("Save Error: " + e.Message);
            throw;
        }
        finally
        {
            fs.Close();
        }
    }

    public static T Load<T>(string dataName)
    {

        if(!File.Exists(savePath + dataName))
        {
            return default(T);
        }
        FileStream fs = new FileStream(savePath + dataName, FileMode.Open);

        T loadData;
        try
        {
            BinaryFormatter bf = new BinaryFormatter();
            loadData = (T)bf.Deserialize(fs);
            return loadData;
        }
        catch (SerializationException e)
        {
            Debug.Log("Load Error: " + e.Message);
            throw;
        }
        finally
        {
            fs.Close();
        }


    }

}
