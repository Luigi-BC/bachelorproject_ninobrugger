﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterTime : MonoBehaviour {

    [SerializeField]
    private float _waitTime;

	// Use this for initialization
	private void Awake ()
    {
        Invoke("KillObject", _waitTime);
	}

    private void KillObject()
    {
        Destroy(gameObject);
    }
	
}
