﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class AnimationCommunication : View<AnimationEventHandler>{

    private Animator _animator;

    private void Start()
    {

//        if(GameStateController.staticInstance.CurrentGameState == GameState.InLevel_Hell)
//        {
//            enabled = false;
//            return;
//        }

        _animator = GetComponent<Animator>();
        Controller = AnimationEventHandler.staticInstance;
        ChangeAnimationState(Controller.AnimationsEnabled);
    }

    protected override void OnControllerChanged(AnimationEventHandler oldController, AnimationEventHandler newController)
    {
        if(oldController != null)
        {
            oldController.EventAnimationStateChanged -= ChangeAnimationState;
        }
        if(newController != null)
        {
            newController.EventAnimationStateChanged += ChangeAnimationState;
        }
    }

    private void ChangeAnimationState(bool state)
    {
        _animator.enabled = state;
    }

    private void OnDestroy()
    {
        if (Controller != null)
        {
            Controller.EventAnimationStateChanged -= ChangeAnimationState;

        }
    }
}
