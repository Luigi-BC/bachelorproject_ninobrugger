﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    [SerializeField]
    private Transform _targetTransform;
    [SerializeField]
    private float _cameraTargetDistance;
    [SerializeField]
    [Range(0, 1)]
    private float _cameraLerping;

	
	// Update is called once per frame
	void LateUpdate ()
    {
        transform.position = Vector3.Lerp(transform.position, new Vector3(_targetTransform.position.x, 2f,_cameraTargetDistance), _cameraLerping);
    }
}
