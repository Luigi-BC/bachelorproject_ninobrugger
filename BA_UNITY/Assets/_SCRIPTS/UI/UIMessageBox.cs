﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMessageBox : MonoBehaviour {

    private static UIMessageBox staticInstance;

    [SerializeField]
    private GameObject _messagePrefab;

    private void Awake()
    {
        if(staticInstance == null)
        {
            staticInstance = this;
        }
    }

    public static void ShowMessage(string messageText, float showTime)
    {
        staticInstance.InstantiateMessage(messageText, showTime);
    }



    public void InstantiateMessage(string messageText, float showTime)
    {
        GameObject currentMessage = Instantiate(_messagePrefab, transform.position, Quaternion.identity, transform);

        MessageController newMessageController = new MessageController();
        newMessageController._messageText = messageText;
        newMessageController._showTime = showTime;
        currentMessage.GetComponent<MessageView>().SetController(newMessageController);
        
    }



}
