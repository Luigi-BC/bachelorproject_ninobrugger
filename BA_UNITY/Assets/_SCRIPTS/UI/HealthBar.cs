﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : View<PlayerHealth>{

    [SerializeField]
    private PlayerHealth controller;


    [SerializeField]
    private Image _fillbar;

	// Use this for initialization
	void Start ()
    {
        Controller = controller;
	}

    protected override void OnControllerChanged(PlayerHealth oldController, PlayerHealth newController)
    {
        if(oldController != null)
        {
            oldController.EventHealthChanged -= OnHealthChanged;
        }

        if(newController != null)
        {
            newController.EventHealthChanged += OnHealthChanged;
        }
    }

    private void OnDestroy()
    {
        if(Controller != null)
        {
            Controller.EventHealthChanged -= OnHealthChanged;
        }
    }


    private void OnHealthChanged(float newValue)
    {
        float newFillAmount = newValue / Controller._startingHealth;

        if(newFillAmount != _fillbar.fillAmount)
        {
            _fillbar.fillAmount = newFillAmount;
        }
    }
	
}
