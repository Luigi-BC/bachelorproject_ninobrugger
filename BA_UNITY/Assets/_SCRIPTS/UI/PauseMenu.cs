﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{

    private bool _menuActive;
    private bool MenuActive
    {
        get { return _menuActive; }
        set
        {
            SetMenuActive(_menuActive = value);

            if(value == true)
            {
                InternalTime.FreezeTime(true);
            }
            else
            {
                InternalTime.UnfreezeTime(true);
            }
        }
    }


    [SerializeField]
    private Canvas _pauseMenuCanvas;
    [SerializeField]
    private Canvas _communicationMenuCanvas;

    [SerializeField]
    private PauseMessage _pauseMessage;
    

    private void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if(InputController.staticInstance.IgnoreAllInput == true) { return; }
            if(MenuActive == false)
            {
                MenuActive = true;
            }
            else
            {
                MenuActive = false;
            }
        }
    }


    private void SetMenuActive(bool state)
    {
        _pauseMenuCanvas.enabled = state;
        if(state == true)
        {
            _communicationMenuCanvas.enabled = HellController.staticInstance.HasEnteredHell;

        }
    }


    public void OnButtonContinue()
    {
        MenuActive = false;
        
    }

    public void OnButtonOptions()
    {

    }

    public async void OnButtonQuitToMenu()
    {
        PauseMessageResult _result = await _pauseMessage.OpenQuestion("Are you sure? All unsaved progress will be lost, although there is no saving system anyway.");
        if(_result == PauseMessageResult.Yes)
        {
            GameStateController.staticInstance.ChangeGameState(GameState.Menu);
        }
    }

    public async void OnButtonQuitGame()
    {
        PauseMessageResult result = await _pauseMessage.OpenQuestion("Do you want the game to ask whether you are sure?");
        
        switch (result)
        {
            case PauseMessageResult.Yes:
                result = await _pauseMessage.OpenQuestion("Are you sure you want to quit?");

                if (result == PauseMessageResult.Yes)
                {
                    Application.Quit();
                    Debug.Log("Quit");
                }

                break;
            case PauseMessageResult.No:
                await _pauseMessage.OpenMessage("Fine.", 0.5f);
                Application.Quit();
                Debug.Log("Quit");

                break;
        }
    }




}
