﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityAsync;
using UnityEngine;
using UnityEngine.UI;

public class PauseMessage : MonoBehaviour {

    [SerializeField]
    private Text _questionText;
    [SerializeField]
    private Canvas _canvas;
    [SerializeField]
    private GameObject _buttons;

    private bool _answerClicked;
    private PauseMessageResult _result;

    private void ChangeCanvasState(bool state)
    {
        _canvas.enabled = state;
    }

    public async Task<PauseMessageResult> OpenQuestion(string _text)
    {
        _answerClicked = false;
        _buttons.SetActive(true);
        _questionText.text = _text;
        ChangeCanvasState(true);

        await Await.Until(() => _answerClicked == true);

        ChangeCanvasState(false);

        return _result;

    }

    public void OnYesClicked()
    {
        _result = PauseMessageResult.Yes;
        _answerClicked = true;
    }

    public void OnNoClicked()
    {
        _result = PauseMessageResult.No;
        _answerClicked = true;
    }

    public async Task OpenMessage(string _text, float _duration)
    {
        _questionText.text = _text;
        _buttons.SetActive(false);
        ChangeCanvasState(true);
        await Task.Delay(TimeSpan.FromSeconds(_duration));
        ChangeCanvasState(false);
    }
}

public enum PauseMessageResult { Yes, No };