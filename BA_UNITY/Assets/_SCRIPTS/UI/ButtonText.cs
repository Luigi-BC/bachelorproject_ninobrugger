﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class ButtonText : MonoBehaviour {



    [SerializeField] private Button _button;
    [SerializeField] private Color _disabledColor;
    [SerializeField] private Color _normalColor;
    private Text _text;

	// Use this for initialization
	private void Awake ()
    {
        _text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	private void Update ()
    {
        _text.color = _button.interactable ? _normalColor : _disabledColor;
	}
}
