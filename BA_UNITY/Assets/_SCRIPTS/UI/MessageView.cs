﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageView : View<MessageController> {

    [SerializeField]
    private Text _text;
    
    
    

    public void SetController(MessageController controller)
    {
        Controller = controller;
    }

    protected override void OnControllerChanged(MessageController oldController, MessageController newController)
    {
        _text.text = newController._messageText;

        WaitAndDestroy();
    }

    private async void WaitAndDestroy()
    {
        await InternalTime.WaitForInternalSeconds(Controller._showTime);
        
        DeleteMessage();
    }
    
    private void DeleteMessage()
    {
        Destroy(gameObject);
    }
}
