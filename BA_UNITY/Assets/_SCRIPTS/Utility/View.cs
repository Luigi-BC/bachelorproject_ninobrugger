﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class View<ControllerType> : MonoBehaviour where ControllerType : class
{
    [SerializeField]
    private ControllerType _controller;
    protected ControllerType Controller
    {
        get { return _controller; }
        set
        {
            OnControllerChanged(_controller, _controller = value);
        }
    }

    public Action<ControllerType, ControllerType> EventControllerChanged = delegate { };

    protected virtual void OnControllerChanged(ControllerType oldController, ControllerType newController)
    {
        EventControllerChanged(oldController, newController);
        Refresh();
    }

    protected virtual void Refresh()
    {

    } 


}
