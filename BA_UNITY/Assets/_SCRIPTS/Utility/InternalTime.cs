﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityAsync;
using UnityEngine;

public class InternalTime : MonoBehaviour
{
    public static float deltaTime;
    public static float timeScale = 1f;
    public static float normalizedTime;

    public static bool timeFrozen;

    private static InternalTime staticInstance;

    private void Awake()
    {
        if(staticInstance == null)
        {
            staticInstance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }


    // Update is called once per frame
    private void Update ()
    {
        deltaTime = Time.unscaledDeltaTime * timeScale;
    }

    #region Freezing
    public static void FreezeTime(bool alsoFreezeInternalTime)
    {
        Time.timeScale = 0f;
        InputController.staticInstance.IgnoreAllInput = true;

        timeFrozen = true;

        if (alsoFreezeInternalTime)
        {
            timeScale = 0f;
        }

    }

    public static async Task WaitForInternalSeconds(float _seconds)
    {
        float _time = 0f;

        while(_time < _seconds)
        {
            await Await.NextUpdate();
            _time += deltaTime;
        }
    }

    public static void UnfreezeTime(bool alsoFreezeInternalTime)
    {
        Time.timeScale = 1f;
        InputController.staticInstance.IgnoreAllInput = false;
        timeFrozen = false;

        if (alsoFreezeInternalTime)
        {
            timeScale = 1f;
        }

    }

    #endregion
    
}
