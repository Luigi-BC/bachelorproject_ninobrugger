﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class SaveLoad
{
    private static string savePath = Application.persistentDataPath + "/";

    public static void SaveData<T>(T saveData, string fileName)
    {
        FileStream fs = new FileStream(savePath + fileName, FileMode.OpenOrCreate);

        try
        {
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs, saveData);
        }
        catch (SerializationException e)
        {
            Debug.LogError("Save Error: " + e.Message);
            throw;
        }
        finally
        {
            fs.Close();
        }
    }

    public static T LoadData<T>(string fileName)
    {
        if (!File.Exists(savePath + fileName))
        {
            return default(T);
        }
        FileStream fs = new FileStream(savePath + fileName, FileMode.Open);

        try
        {
            BinaryFormatter bf = new BinaryFormatter();
            return (T) bf.Deserialize(fs);
        }
        catch (SerializationException e)
        {
            Debug.LogError("Load Error: " + e.Message);
            throw;
        }
        finally
        {
            fs.Close();
        }

    }
	
}
